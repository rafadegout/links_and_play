<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BatimentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nomBatiment' => Str::random(10),
            'numeroBatiment' => integer::unique()->random(999),
            'DateBatiment' =>DateTime::format(),
        ];
    }
}
