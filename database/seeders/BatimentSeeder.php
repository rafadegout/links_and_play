<?php

use App\Models\batiment;
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BatimentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Batiment::factory()
       ->count(50)
       ->hasPosts(1)
       ->create();
    }
}
