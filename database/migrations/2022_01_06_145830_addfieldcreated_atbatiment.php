<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddfieldcreatedAtbatiment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('batiments', function (Blueprint $table)//
        {
            $table->timestamp('created_at')->nullable();
        }); // //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('batiments', function (Blueprint $table)//
        {
            $table->dropcolumn('created_at');
        }); ////
    }
}
