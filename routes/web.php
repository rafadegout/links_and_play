<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\AnnonceController;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

//debut route utilisateurs

Route::middleware(['auth','UserIsAdmin'])->group(function (){

Route::post('/user/store', [UserController::class, 'store'])->name('UserStore');

Route::get('/user/formulaire-ajout', [UserController::class, 'create'])->name('usercreate');

Route::get('/user/userlist/{option}', [UserController::class, 'index'])->name('userlist');

Route::get('/user/userdelete/{user_id}', [UserController::class, 'destroy'])->name('userdelete');

Route::get('/user/useredit/{user_id}', [UserController::class, 'edit'])->name('useredit');

Route::get('/user/userkill/{user_id}', [UserController::class, 'kill'])->name('userkill');

Route::get('/user/userrestore/{user_id}', [UserController::class, 'restore'])->name('userrestore');

Route::post('/user/update', [UserController::class, 'update'])->name('userupdate');

//fin route utilisateurs

//debut route games


Route::post('/game/store', [GameController::class, 'store'])->name('GameStore');

Route::get('/game/formulaire-ajout', [GameController::class, 'create'])->name('gamecreate');

Route::get('/game/gamelist/{option}', [GameController::class, 'index'])->name('gamelist');

Route::get('/game/gamedelete/{game_id}', [GameController::class, 'destroy'])->name('gamedelete');

Route::get('/game/gameedit/{game_id}', [GameController::class, 'edit'])->name('gameedit');

Route::get('/game/gamekill/{game_id}', [GameController::class, 'kill'])->name('gamekill');

Route::get('/game/gamerestore/{game_id}', [GameController::class, 'restore'])->name('gamerestore');

Route::post('/game/update', [GameController::class, 'update'])->name('gameupdate');

// fin route games
});

//debut route annonces

Route::middleware(['auth'])->group(function (){

    Route::post('/annonce/store', [AnnonceController::class, 'store'])->name('AnnonceStore');

    Route::get('/annonce/create', [AnnonceController::class, 'create'])->name('Annoncecreate');

    Route::get('/annonce/list/{option}', [AnnonceController::class, 'index'])->name('Annoncelist');

    route::get('/annonce/delete/{annonce_id}', [AnnonceController::class, 'destroy'])->name('annoncedestroy');

    route::get('/annonce/edit/{annonce_id}',[AnnonceController::class, 'edit'])->name('annonceedit');

    route::post('/annonce/update',[AnnonceController::class, 'update'])->name('annonceupdate');

    Route::get('/annonce/annoncekill/{annonce_id}', [AnnonceController::class, 'kill'])->name('annoncekill');

    Route::get('/annonce/annoncerestore/{annonce_id}', [AnnonceController::class, 'restore'])->name('annoncerestore');
});

// fin route annonces
