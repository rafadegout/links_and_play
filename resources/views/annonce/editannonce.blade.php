@extends('layouts.buildanddestroy')
@section('main')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
   <form action='{{route('annonceupdate')}}' method="post">
   <input type='hidden' value={{$annonce->id}} name='id'>
    {{csrf_field()}}

    {{-- @if(Auth->game()->gameisAdmin())
    <input name="id" value="{{$game->id}}">
    @endif --}}

  <div class="mb-3">
    <label>Titre de l'annonce</label>
   <input type="texte" name="titre" value="{{ old('titre', $annonce->titre)}}" placeholder="titre">
  </div>
  <div class="mb-3">
    <label>numero</label>
    <input type="number" required  name="numero">
  </div>
   <div class="mb-3">
    <label>Creez le</label>
    <input type="datetime-local" name="created_at" value="{{ old('created_at', $annonce->created_at)}}">
  </div>
   <div class="mb-3">
    <label>Description</label>
       <textarea type="texte" name="description" value="{{ old('description', $annonce->description)}}" placeholder="bio"></textarea>
  </div>
  <div class="mb-3 form-check">
    <input type="checkbox" required class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">comfirm</label>
  </div>
  <input type="submit" value="Validé">
  <a href="{{route('gamelist',['option'=>'tous'])}}"><button type="button" >liste des games</button></a>
  
</form>
 @if (\Session::has('success'))
              {!! \Session::get('success') !!}
      @elseif ('une erreur est survenue')
    @endif
@endsection