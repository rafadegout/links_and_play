@extends('layouts.buildanddestroy')
@section('main')

<table class="table table-striped table-sm">
  <thead>
    <tr>
      <th>Titre</th>
      <th>Description</th>
      <th>Creez le</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
      @foreach($annonces as $annonce)
          <tr class="table-primary">
              <td>{{ $annonce->titre }}</td>
              <td>{{ Str::limit($annonce->description,30) }}</td>
              <td>{{ $annonce->created_at }}</td>


              @if ($annonce->deleted_at == null)
                <td><a href="{{route('annoncedestroy',['annonce_id'=>$annonce->id])}}"><button type="button" class="btn btn-danger">delete</button></a></td>
                <td><a href="{{route('annonceedit',['annonce_id'=>$annonce->id])}}"><button type="button" class="btn btn-warning">edit</button></a></td>
              @else
                <td><a href="{{route('annoncekill',['annonce_id'=>$annonce->id])}}"><button type="button" class="btn btn-danger">detruire</button></a></td>
                <td><a href="{{route('annoncerestore',['annonce_id'=>$annonce->id])}}"><button type="button" class="btn btn-warning">restaurer</button></a></td>
              @endif
              
            
          </tr>
      @endforeach
  </tbody>
  
  </table>
  

   
  <a href="{{'/annonce/create'}}"><button type="button" class="btn btn-outline-info">add</button></a>
  <a href="{{route('Annoncelist',['option'=>'ok'])}}"><button type="button" class="btn btn-outline-danger">present</button></a>
  <a href="{{route('Annoncelist',['option'=>'corbeille'])}}"><button type="button" class="btn btn-outline-danger">corbeille</button></a>
  <a href="{{route('Annoncelist',['option'=>'tous'])}}"><button type="button" class="btn btn-outline-danger">tous</button></a>
@endsection