@extends('layouts.buildanddestroy')
@section('main')

<table class="table table-striped table-sm">
  <thead>
    <tr>
      <th>Nom</th>
      <th>Biographie</th>
      <th>Date de naissance</th>
      
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
      @foreach($users as $user)
          <tr class="table-primary">
              <td>{{$user->name}}</td>
              <td>{{$user->biographie}}</td>
              <td>{{$user->dateNaissance != ''? $user->dateNaissance->format('D m y'):'' }} ({{$user->dateNaissance !=''? $user->dateNaissance->diffForHumans():'valeur inconnue'}})</td>



              @if ($user->deleted_at == null)
                <td><a href="{{route('userdelete',['user_id'=>$user->id])}}"><button type="button" class="btn btn-danger">delete</button></a></td>
                <td><a href="{{route('useredit',['user_id'=>$user->id])}}"><button type="button" class="btn btn-warning">edit</button></a></td>
              @else
                <td><a href="{{route('userkill',['user_id'=>$user->id])}}"><button type="button" class="btn btn-danger">detruire</button></a></td>
                <td><a href="{{route('userrestore',['user_id'=>$user->id])}}"><button type="button" class="btn btn-warning">restaurer</button></a></td>
              @endif
              
            
          </tr>
      @endforeach
  </tbody>
  
  </table>
  

   
  <a href="{{'/user/formulaire-ajout'}}"><button type="button" class="btn btn-outline-info">add</button></a>
  <a href="{{route('userlist',['option'=>'ok'])}}"><button type="button" class="btn btn-outline-danger">present</button></a>
  <a href="{{route('userlist',['option'=>'corbeille'])}}"><button type="button" class="btn btn-outline-danger">corbeille</button></a>
  <a href="{{route('userlist',['option'=>'tous'])}}"><button type="button" class="btn btn-outline-danger">tous</button></a>
@endsection