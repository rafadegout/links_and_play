@extends('layouts.buildanddestroy')
@section('main')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
   <form  enctype="multipart/form-data" action='{{route('userupdate')}}' method="post">
   <input type='hidden' value={{$user->id}} name='id'>
    {{csrf_field()}}

  <div class="mb-3">
    <label>Pseudo</label>
   <input type="texte" name="name" value="{{ old('name', $user->name)}}" placeholder="name">
  </div>
  <div class="mb-3">
    <label>Mot de passe</label>
    <input type="password" required  name="password">
  </div>
  <div class="mb-3">
    <label>Comfirmation du mot de passe</label>
    <input type="password" required  name="password_confirmation">
  </div>
   <div class="mb-3">
    <label>Date de naissance</label>
    <input type="datetime-local" name="dateNaissance" value="{{ old('dateNaissance', $user->dateNaissance)}}" placeholder="date de naissance">
  </div>
   <div class="mb-3">
    <label>Biographie</label>
       <textarea type="texte" name="biographie" placeholder="bio">{{ old('biographie', $user->biographie)}}</textarea>
  </div>

  <label for="avatar">image de profil:</label>
    <input type="file"
          id="avatar" name="avatar"
          accept="image/png, image/jpeg">
  </div>

  <div class="mb-3 form-check">
    <input type="checkbox" required class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">comfirm</label>
  </div>
  <input type="submit" value="Validé">
  <a href="{{route('userlist',['option'=>'tous'])}}"><button type="button" >liste des users</button></a>
  
</form>
 @if (\Session::has('success'))
              {!! \Session::get('success') !!}
      @elseif ('une erreur est survenue')
    @endif
@endsection