@extends('layouts.buildanddestroy')
@section('main')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
   <form action='/user/store' method="post">
    {{csrf_field()}}
  <div class="mb-3">
    <label>Pseudo</label>
    <input type="texte" required  name="name" placeholder="name">
  </div>
  <div class="mb-3">
    <label>Mot de passe</label>
    <input type="password" required  name="password">
  </div>
  <div class="mb-3">
    <label>Comfirmation du mot de passe</label>
    <input type="password" required  name="password_confirmation">
  </div>
  
   <div class="mb-3">
    <label>Date de naissance</label>
    <input type="datetime-local" required  name="dateNaissance"  placeholder="datedenaissance">
  </div>
   <div class="mb-3">
    <label>Biographie</label>
    <textarea type="texte" name="biographie" placeholder="description"> </textarea>
  </div>
  <div>
  <label for="avatar">image de profil:</label>
    <input type="file"
          id="avatar" name="avatar"
          accept="image/png, image/jpeg">
  </div>
  <div class="mb-3 form-check">
    <input type="checkbox" required class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">comfirm</label>
  </div>
  

  <input type="submit" value="Validé">
  <a href="{{route('userlist',['option'=>'tous'])}}"><button type="button" >liste des users</button></a>
  
</form>
 @if (\Session::has('success'))
              {!! \Session::get('success') !!}
      @elseif ('une erreur est survenue')
    @endif
@endsection