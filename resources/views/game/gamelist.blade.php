@extends('layouts.buildanddestroy')
@section('main')

<table class="table table-striped table-sm">
  <thead>
    <tr>
      <th>Nom</th>
      <th>Biographie</th>
      <th>Creez le</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
      @foreach($games as $game)
          <tr class="table-primary">
              <td>{{ $game->name }}</td>
              <td>{{ Str::limit($game->biographie,30) }}</td>
              <td>{{ $game->created_at }}</td>


              @if ($game->deleted_at == null)
                <td><a href="{{route('gamedelete',['game_id'=>$game->id])}}"><button type="button" class="btn btn-danger">delete</button></a></td>
                <td><a href="{{route('gameedit',['game_id'=>$game->id])}}"><button type="button" class="btn btn-warning">edit</button></a></td>
              @else
                <td><a href="{{route('gamekill',['game_id'=>$game->id])}}"><button type="button" class="btn btn-danger">detruire</button></a></td>
                <td><a href="{{route('gamerestore',['game_id'=>$game->id])}}"><button type="button" class="btn btn-warning">restaurer</button></a></td>
              @endif
              
            
          </tr>
      @endforeach
  </tbody>
  
  </table>
  

   
  <a href="{{'/game/formulaire-ajout'}}"><button type="button" class="btn btn-outline-info">add</button></a>
  <a href="{{route('gamelist',['option'=>'ok'])}}"><button type="button" class="btn btn-outline-danger">present</button></a>
  <a href="{{route('gamelist',['option'=>'corbeille'])}}"><button type="button" class="btn btn-outline-danger">corbeille</button></a>
  <a href="{{route('gamelist',['option'=>'tous'])}}"><button type="button" class="btn btn-outline-danger">tous</button></a>
@endsection