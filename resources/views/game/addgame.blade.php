@extends('layouts.buildanddestroy')
@section('main')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
   <form action='/game/store' method="post">
    {{csrf_field()}}
  <div class="mb-3">
    <label>nom du jeux</label>
    <input type="texte" required  name="name" placeholder="name">
  </div>
  <div class="mb-3">
    <label>numero</label>
    <input type="number" required  name="numero">
  </div>
   <div class="mb-3">
    <label>Creez le</label>
    <input type="datetime-local" required  name="created_at">
  </div>
   <div class="mb-3">
    <label>Biographie</label>
    <textarea type="texte" name="biographie" placeholder="description"> </textarea>
  </div>
  <div class="mb-3 form-check">
    <input type="checkbox" required class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">comfirm</label>
  </div>
  <input type="submit" value="Validé">
  <a href="{{route('gamelist',['option'=>'tous'])}}"><button type="button" >liste des jeux</button></a>
  
</form>
 @if (\Session::has('success'))
              {!! \Session::get('success') !!}
      @elseif ('une erreur est survenue')
    @endif
@endsection