@extends('layouts.buildanddestroy')
@section('main')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

   <form action='{{route('gameupdate')}}' method="post">
    {{csrf_field()}}
    <input type="hidden" name="id" value='{{$game->id}}'>
    {{-- @if(Auth->game()->gameisAdmin())
    <input name="id" value="{{$game->id}}">
    @endif --}}

  <div class="mb-3">
    <label>nom du jeux</label>
   <input type="texte" name="name" value="{{ old('name', $game->name)}}" placeholder="name">
  </div>
  <div class="mb-3">
    <label>numero</label>
    <input type="number" required  name="numero">
  </div>
   <div class="mb-3">
    <label>Creez le</label>
    <input type="datetime-local" name="created_at" value="{{ old('created_at', $game->created_at)}}">
  </div>
   <div class="mb-3">
    <label>Biographie</label>
       <textarea type="texte" name="biographie" value="{{ old('biographie', $game->biographie)}}" placeholder="bio"></textarea>
  </div>
  <div class="mb-3 form-check">
    <input type="checkbox" required class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">comfirm</label>
  </div>
  <input type="submit" value="Validé">
  <a href="{{route('gamelist',['option'=>'tous'])}}"><button type="button" >liste des games</button></a>
  
</form>
 @if (\Session::has('success'))
              {!! \Session::get('success') !!}
      @elseif ('une erreur est survenue')
    @endif
@endsection