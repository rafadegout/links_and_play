<?php

namespace App\Models;

use App\Models\User;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\TraitUuid;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasFactory, SoftDeletes;
    use \App\Traits\TraitUuid;

    //  protected $dates=['dateNaissance'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email', 
        'Biographie',
    ];


    protected $dates = [
        
        'dateNaissance'

    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function annonces()
    {
        return $this->belongsToMany( 'App\Models\Annonce', 'annonce_user' )->withPivot(['administrateur']);
    }

    public function isAdmin(){
        return $this->administrateur;
    }

}
