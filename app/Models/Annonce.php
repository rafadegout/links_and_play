<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\TraitUuid;
use Illuminate\Database\Eloquent\SoftDeletes;


class Annonce extends Model
{
    use HasFactory;
    use \App\Traits\TraitUuid;
    use Softdeletes;

    protected $fillable = [
        'titre', 
        'description',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    public function users()
    {
        return $this->belongsToMany( 'App\Models\User' )->withPivot(['id','administrateur']);
    }
}
