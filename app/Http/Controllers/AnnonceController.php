<?php

namespace App\Http\Controllers;
use App\Http\Requests\StoreAnnonce;
use App\Models\Game;
use App\Models\Annonce;
use Illuminate\Http\Request;
use Illuminate\support\facades\Auth;
use Illuminate\Support\Str;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($option = null)
    {
        switch($option)
        {

            case 'corbeille': // affiche les données soft delete
                $annonces = annonce::onlyTrashed()->get(); //   
            break;
        

            case 'tous': // affiche toute les données soft delete et non soft delete
                $annonces = annonce::withTrashed()->get(); //
            break; 
        
   
            default: // affiche les données non soft delete
                $annonces = annonce::all(); //  
        }
        
        return view('annonce.listannonce',compact('annonces')); //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $games= Game::All();
        return view('annonce/addannonce',compact('games'));//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $games = Game::where('name', $request->game)->first();
        if($games == false){
            return back()->with('danger', 'ce jeux n\'existe pas ,selectionné un jeux dans la liste');
        }
        $annonce = new Annonce;
        $annonce->game_id = $games->id;
        $annonce->titre = $request->titre;
        $annonce->description = $request->description;
        $annonce->created_at = $request->created_at;

        // dd($game);
        $annonce->save();
        $annonce->users()->attach(
            [ Auth::user()->id => [ 
                'id'=>Str::uuid(),
                'administrateur' => 1 ]]
        );
    
        return back()->with('success','builders have add the player '.$annonce->titre.'!');//
    }


    
    public function restore($id)
    {   
       
        $annonce = Annonce::where('id',$id)->withTrashed()->first();
        $annonce->restore(); //

        return back()->with('success','the builders have well restore game '.$annonce->titre.'!');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        {
            $annnonce= annonce::findOrFail($annonce_id);//
    
            return view('editannonce', compact('annonce'));////
        }//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($annonce_id)
    {
        $annonce = annonce::findOrfail($annonce_id);
        
        return view('annonce.editannonce',compact('annonce'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $annonce = Annonce::findOrFail( $request->id );//

        $annonce->titre = $request->titre;
        $annonce->description = $request->description;

        $annonce->update();
        
        return back()->with('success','the builders have edit the user !');
         //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $annonce = Annonce::findOrFail($id);
        
        $annonce->delete(); //

        return back()->with('danger','the builders have well delete user !');//
    }
}
