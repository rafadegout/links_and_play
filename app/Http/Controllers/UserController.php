<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUser;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use  Illuminate\support\facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($option = null)
    {
        switch($option)
        {

            case 'corbeille': // affiche les données soft delete
                $users = user::onlyTrashed()->get(); //   
            break;
        

            case 'tous': // affiche toute les données soft delete et non soft delete
                $users = user::withTrashed()->get(); //
            break; 
        
   
            default: // affiche les données non soft delete
                $users = user::all(); //  
        }
        
        return view('user/listuser',compact('users'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user/adduser');////
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $user = new User;
    
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->biographie = $request->biographie;
        $user->dateNaissance = $request->dateNaissance;
        // dd($user);
        $user->save();

        return back()->with('success','builders have add the player '.$user->name.'!');//
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user= user::findOrFail($user_id);//

        return view('useredit', compact('user'));////
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        $user = user::findOrFail($user_id);//
        

        return view('user.useredit', compact('user'));////
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(request $request)
    {
        $isAdmin = Auth::user()->isAdmin();
        $user_id = null;
        
       
        if($isAdmin == true){
            $user_id = $request->id;
        }
        else{
            $user_id = Auth::user()->id;
        }
        
        $user = user::findOrFail($user_id);//select * from user where id = $user_id

        // $user = user::findOrFail(Auth::user()->isAdmin() ? $request->id : Auth::user()->id );//

        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->biographie = $request->biographie;
        $user->dateNaissance = $request->dateNaissance;

        $user->update();
        
        return redirect()->action([UserController::class,'index'],['option'=>'salut'])->with('success','the builders have edit the user !');
         
    }

    public function updateProfil(request $request)
    {

        $user = user::findOrFail(Auth::user()->id);

        // dd($user);
        $user->name = $request->name;
        $user->biographie = $request->biographie;
        $user->dateNaissance = $request->dateNaissance;

        $user->update();
        
        return redirect()->action([UserController::class,'index'],['option'=>'salut'])->with('success','the builders have edit the user !');
         //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = user::findOrFail($id);
        
        $user->delete(); //

        return back()->with('danger','the builders have well delete user !');//
    }

    public function restore($id)
    {   
       
        $user = user::where('id',$id)->withTrashed()->first();
        $user->restore(); //

        return back()->with('success','the builders have well restore user '.$user->name.'!');
    }

    public function kill($id)
    {
        $user = user::where('id',$id)->withTrashed()->first();

        
        $user->forceDelete(); //supprime de la corbeille l'utilisateur' soft delete

        return back()->with('danger','the builders have well destroy user '.$user->name.' on database !');
        
    }
    
}
