<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Storegame;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GameController;
use App\Models\game;
use  Illuminate\support\facades\Auth;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($option = null)
    {
        switch($option)
        {

            case 'corbeille': // affiche les données soft delete
                $games = game::onlyTrashed()->get();//   
            break;
        

            case 'tous': // affiche toute les données soft delete et non soft delete
                $games = game::withTrashed()->get();//
            break; 
        
   
            default: // affiche les données non soft delete
                $games = game::all();//  
        }

       
        // dd($games);
        return view('game/gamelist',compact('games'));
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('game/addgame');//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Storegame $request)
    {
        $game = new game;
    
        $game->name = $request->name;
        $game->numero = $request->numero;
        $game->biographie = $request->biographie;
        $game->created_at = $request->created_at;
        // dd($game);
        $game->save();

        return back()->with('success','builders have add the player '.$game->name.'!');//
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $game= game::findOrFail($game_id);//

        return view('gameedit', compact('game'));//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($game_id)
    {
        $game = game::findOrFail($game_id);//

        return view('game.gameedit', compact('game'));//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(request $request)
    {
        // dd($request->all());
        $game = game::findOrFail( $request->id );//

        // dd($game);
        $game->name = $request->name;
        $game->biographie = $request->biographie;
        $game->created_at = $request->created_at;

        $game->update();
        
        return back()->with('success','the builders have edit the game !');
         //
    }

    public function updateProfil(request $request)
    {

        $game = game::findOrFail(Auth::game()->id);

        // dd($game);
        $game->name = $request->name;
        $game->biographie = $request->biographie;
        $game->created_at = $request->created_at;

        $game->update();
        
        return redirect()->action([gameController::class,'index'],['option'=>'salut'])->with('success','the builders have edit the game !');
         //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $game = game::findOrFail($id);
        
        $game->delete(); //

        return back()->with('danger','the builders have well delete game !');//
    }

    public function restore($id)
    {   
       
        $game = game::where('id',$id)->withTrashed()->first();
        $game->restore(); //

        return back()->with('success','the builders have well restore game '.$game->name.'!');
    }

    public function kill($id)
    {
        $game = game::where('id',$id)->withTrashed()->first();

        
        $game->forceDelete(); //supprime de la corbeille l'utilisateur' soft delete

        return back()->with('danger','the builders have well destroy game '.$game->name.' on database !');
        
    }
    
}
