<?php

namespace App\Http\Requests;

use App\Http\Requests\StoreGame;
use Illuminate\Foundation\Http\FormRequest;

class StoreGame extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'name' => 'required|max:255',
            'biographie' => 'max:255', 
            'Created_at'=>'date',
            'id'=> 'exists:games,id'
        ];
    }
}
