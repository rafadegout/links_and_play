<?php

namespace App\Http\Requests;
use App\Http\Controllers\Auth;

use Illuminate\Foundation\Http\FormRequest;

class StoreBatiment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'name' => 'required|max:255',
            'numero' => 'required|numeric|max:999', 
            'created_at'=>'required|date',
        ];
    }
}
